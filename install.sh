#!/bin/bash


## This script install mmmpuppet to your device. Be noted that it will also remove some conflicting depencies. This project is a fork from: https://gitlab.com/untidylamp/mmmpuppet

git clone https://gitlab.com/untidylamp/mmmpuppet
cd mmmpuppet


text="Nice to meet you - I am the famous ModemManager Matrix Puppet Bridge aka mmmpuppet
I will guide you trough installation and if everything goes well you can just forget me and enjoy my work
There are some things to check to make sure I can work reliable

1. You have to have atleast two matrix accounts - one bot account for me and one user account for you

2. You are installing me to linux device which runs ModemManager and you have modem with working sim connected to that device (example Pinephone or linux pc with usb modem)

3. I can not run if the device where I live is suspended - how ever if the device can wake up from suspend when receiving SMS/MMS it will wake me too

4. I need internet connection to bridge SMS/MMS to matrix - without it the bridge does not work

So lets start configuration
I need some details so I can start bridging your SMS/MMS to your matrix server"
for (( i=0; i<${#text}; i++ )); do
    echo -n "${text:$i:1}"
    sleep 0.01
done
echo ""

##Matrix server
text="Tell me your matrix homeserver
example
https://matrix-client.matrix.org"
for (( i=0; i<${#text}; i++ )); do
    echo -n "${text:$i:1}"
    sleep 0.01
done
echo ""

read homeserver

text="Your matrix homeserver is 
$homeserver

"
for (( i=0; i<${#text}; i++ )); do
    echo -n "${text:$i:1}"
    sleep 0.01
done
echo ""

##Bot account
text="I need to have my own bot account and I use it to communicate with your matrix server
Tell me my bot account username 
example 
@smsbot:matrix.org"
for (( i=0; i<${#text}; i++ )); do
    echo -n "${text:$i:1}"
    sleep 0.01
done
echo ""

read bot_account

text="My bot account username is: 
$bot_account

"
for (( i=0; i<${#text}; i++ )); do
    echo -n "${text:$i:1}"
    sleep 0.01
done
echo ""

##Bot password
text="To use my bot account I need also the password 
I will remove the password on first run
Tell me my bot accout password"
for (( i=0; i<${#text}; i++ )); do
    echo -n "${text:$i:1}"
    sleep 0.01
done
echo ""

read bot_password

text="My bot account password is: 
$bot_password

"
for (( i=0; i<${#text}; i++ )); do
    echo -n "${text:$i:1}"
    sleep 0.01
done
echo ""


##Device name
text="Tell me by which name you want Matrix to recognize this device"
for (( i=0; i<${#text}; i++ )); do
    echo -n "${text:$i:1}"
    sleep 0.01
done
echo ""

read device_name

text="Device name for this device is: 
$device_name

"
for (( i=0; i<${#text}; i++ )); do
    echo -n "${text:$i:1}"
    sleep 0.01
done
echo ""

##Users 
text="Tell me all the user who I invite to SMS/MMS rooms
Note that everyone will have same access to read and write messages

Example for one user 
@smstest:matrix.org

Example for multiply users
@smstest:matrix.org,@smstest2:matrix.org,@smstest3:matrix.org"
for (( i=0; i<${#text}; i++ )); do
    echo -n "${text:$i:1}"
    sleep 0.01
done
echo ""

read matrix_users_csv

text="I am inviting: 
$matrix_users_csv

"
for (( i=0; i<${#text}; i++ )); do
    echo -n "${text:$i:1}"
    sleep 0.01
done
echo ""

##Phonenumber
text="Tell me the phone number of this device
It is the number which I will use and the SIM must be in this same device
example 
+358400123456"
for (( i=0; i<${#text}; i++ )); do
    echo -n "${text:$i:1}"
    sleep 0.01
done
echo ""

read cell_number

text="My phone number is: 
$cell_number


"
for (( i=0; i<${#text}; i++ )); do
    echo -n "${text:$i:1}"
    sleep 0.01
done
echo ""

##Cell country
text="Tell me country code of my phone number
example 
FI"
for (( i=0; i<${#text}; i++ )); do
    echo -n "${text:$i:1}"
    sleep 0.01
done
echo ""

read cell_country

text="My country code is: 
$cell_country


"
for (( i=0; i<${#text}; i++ )); do
    echo -n "${text:$i:1}"
    sleep 0.01
done
echo ""

##MMS size
text="Tell me MMS size limit - if you are not sure just write the example value
Example 
650000"
for (( i=0; i<${#text}; i++ )); do
    echo -n "${text:$i:1}"
    sleep 0.01
done
echo ""

read mms_size_limit

text="MMS size limit is: 
$mms_size_limit


"
for (( i=0; i<${#text}; i++ )); do
    echo -n "${text:$i:1}"
    sleep 0.01
done
echo ""


# Install general dependencies and remove unneeded
text="First configuration step is done - let's"
for (( i=0; i<${#text}; i++ )); do
    echo -n "${text:$i:1}"
    sleep 0.01
done
echo ""
sudo pacman -Syyuu
sudo pacman -S --noconfirm python-pip
pip3 install matrix-nio phonenumbers vobject --user
mkdir ~/.config/mmm/
sudo killall chatty 
sudo pacman -R chatty
if [ -z $(which mmsdtng) ]; then
sudo pacman -S mmsd-tng
systemctl start mmsd-tng --user
fi




# Config file and initial run
cat <<-'EOF' > ~/.config/mmm/conf.json
{
"homeserver": "$homeserver",
"bot_account": "$bot_account",
"bot_password": "$bot_password",
"device_name": "$device_name",
"matrix_users_csv": "$matrix_users_csv",
"cell_number": "$cell_number",
"cell_country": "$cell_country",
"mms_size_limit": "$mms_size_limit"
}
EOF

./mmmpuppet.py
sudo cp *py /usr/local/bin/

# Setup systemd unit file
cat <<-'EOF' > /tmp/mmmpuppet.service
[Unit]
Description=Starts mmmpuppet interface
After=mmsd-tng.service

[Service]
ExecStart=/usr/bin/python3 /usr/local/bin/mmmpuppet.py
Restart=on-failure
RestartSec=10s

[Install]
WantedBy=default.target
EOF

sudo mv /tmp/mmmpuppet.service /usr/lib/systemd/user/
sudo chmod 644 /usr/lib/systemd/user/mmmpuppet.service
sudo systemctl daemon-reload
systemctl enable mmmpuppet.service --user
systemctl start mmmpuppet.service --user
